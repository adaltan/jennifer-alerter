# Jennifer Alerter

	- 2012/12/04 윤종혁.


## 소개.

	- 경보 warn, critical 그룹과 세부 경보에 따라서 액션 설정이 가능.
	- 썬더메일을 이용한 이메일 및 SMS 발송.
	- DB 테이블에 경보 내역을 저장.
	- 경보 내용 통지, 기록을 설정을 통해서 어떤 단계나 어떤 경보는 통지할지, 기록할지 설정 파일로 설정 가능.
	- 경보 이메일, SMS 메시지의 내용을 JMTE 템플릿으로 간단히 설정 가능.
	


## 개발환경: 필요사항.
	
	- JDK 1.6+
	- Eclipse
	- Eclipse + Maven Integration (Juno이상은 내장) : http://www.sonatype.org/m2eclipse/

	
## 개발환경: 프로젝트 세팅.

	1. mvn/install-file-to-local-mvn-artifacts.cmd 스크립트 실행하여 상용 JAR등을 로컬 저장소에 등록. (Maven 연동이 가능하도록.)
	2. 단순히 이클립스에서 "Import Project / Existing Maven Project"으로 가져오기하여 프로젝트 세팅.

	

## 빌드

	- 터미널에서 mvn package을 실행하거나.
	- "package" Maven Target을 실행하거나. 
	- launchers/jennifer-alerter package.launch 파일을 이클립스에서 실행한다.
	
	

## 배치

	- 어떤 서버에.
		- 자세한 내용은 j2.pantech.com에 대한 서버 접속 정보를 확인하세요. (docs 디렉토리).
			
	- 어떤 경로에.
		- (상동)
	
	- 어떤 설정 파일에.
		- (상동)
	




## 주요 설정 파일.

	1. 메인 설정 파일
		
		주요 설정 파일들은 다음 파일을 시작으로.
			- src/main/resources/app-config-files.xml 
			- 위 파일에 지정된 하위 설정 파일들을 모두 로딩함.
			- 보통 src/main/resources/app-config 저장된 하위 설정 파일들로 이루어짐.
			
	
	
	2. 경보 레벨 설정
	
		경보는 크게 두가지 기준으로 분류함.
			1. 경보 그룹 : MESSAGE, WARN, ERROR, CRITICAL
			2. 상세 경보 타입 :  WARNING_JVM_CPU_HIGH, ERROR_UNCAUGHT_EXCEPTION 등...
				- 상세 경보 타입은 com.javaservice.jennifer.util.Def 에 상수들로 정의되어있음. (레퍼런스 참고.)
	
		경보에 대한 액션은 크게 세가지.
			1. 이메일 발송. (email)
			2. SMS 발송. (sms)
			3. DB로깅. (logging)
			
		
		위의 두 내용을 조합하여, 설정 파일 alert-levels.xml에 다음과 같은 형태로 지정함. (src/main/resources/app-config 디렉토리)
			
				<!-- CRITICAL, ERROR, WARNING, MESSAGE 경보 레벨은 logging하도록. -->
				<type-groups.logging>
					CRITICAL, ERROR, WARNING, MESSAGE
				</type-groups.logging>
		
				<!-- WARNING_JVM_CPU_HIGH 경보는 sms을 발송.  -->
				<types.sms>WARNING_JVM_CPU_HIGH</types.sms>
				
				이런 type-groups/types와 logging/sms/email의 조합을한 <type-groups.logging>, <types.sms>와 같은 태그 안에 원하는 경보 그룹이나 경보 이름을 적어 해당 경보 그룹이나 경보에 어떤 액션을 취할지 지정한다. (logging, sms, email)
		
		
		

	
	3. 메인 DB 및 주요 쿼리
	
		메인 DB 접속은 
			- src/main/resources/mybatis-config.xml 파일에서 설정.
			
		
		수신 대상자를 나열하는 쿼리는
			- src/main/resources/lots/jennifer_alerter/sqlmaps/recipients.xml 의 listAll을 수정하여 변경.
			
	
	
	
	4. SMS 관련
	
		SMS은 메인 DB의 다음 테이블에 SQL INSERT하여 발송.
			- EM_SMT_TRAN
			- 해당 쿼리는 src/main/resources/lots/jennifer_alerter/sqlmaps/sms.xml 파일의 sendSms 쿼리로 변경가능.
	
		SMS의 본문의 내용은 src/main/resources/lots/jennifer_alerter/templates/sms.jmte.txt 파일을 수정하여 템플릿 수정가능.
			- 참고: http://code.google.com/p/jmte/
		
		설정 파일은 src/main/resources/app-config/sms.xml
			- 위 파일을 수정하여 발신 전화번호 설정 가능.

		
		
	
	5. 이메일 관련
	
		이메일은 thundermail 서버에서 발송.
		
		설정 파일은 src/main/resources/app-config/thundermail.xml
			- 발신시 사용할 thundermail 서버 주소.
			- 발신자명, 발신자 이메일 주소.
			- 발신시 사용할 thundermail 서버에 설정한 템플릿ID.
		
		 이메일 제목과 내용에 대한 템플릿 파일.
			- src/main/resources/lots/jennifer_alerter/templates 디렉토리.
			- thundermail-content.jmte.txt : 메일 본문 내용 텍스트.
			- thundermail-title.jmte.txt : 메일 제목 텍스트.
			- 참고: http://code.google.com/p/jmte/
	
	
	
	
	6. 내역 디비
	
		경보 내역이 다음의 메인 DB 테이블에 쌓임.
			- SM_JENNIFER_ALERT
	
		이들 테이블을 처음 생성할때는 다음 DDL 스크립트들을 활용하것.
			- sql/01. create seq_sm_jennifer_alert.sql
			- sql/02. create sm_jennifer_alert.sql

		해당 쿼리는
			- src/main/resources/lots/jennifer_alerter/sqlmaps/dbLogging.xml 파일의 insert 쿼리를 수정하여 변경.
	
	
	
	7. 경보 내용에 따라 필터링
	
		경보 내용을 통해 자바스크립트로 경보를 통지하거나 하지 않도록 설정 가능.
			- src/main/resources/lots/jennifer_alerter/error_filters/RhinoErrorFilter.js
				- error.agentname이 W, S, P으로 시작하거나 아예 없을때만 통지하도록 설정되어있음.
			


###끝