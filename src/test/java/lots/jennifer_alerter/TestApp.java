package lots.jennifer_alerter;

import java.io.IOException;

import lots.jennifer_alerter.alert_actors.DbLoggingAlertActor;
import lots.jennifer_alerter.injections.Guicer;
import lots.jennifer_alerter.logging.InjectLogger;
import lots.jennifer_alerter.sqlmaps.HelloMapper;
import lots.jennifer_alerter.sqlmaps.RecipientsMapper;
import lots.jennifer_alerter.sqlmaps.SmsMapper;
import lots.jennifer_alerter.templates.JmteExpanderWithErrorObject;
import lots.jennifer_alerter.thundermail.ThundermailSender;
import lots.jennifer_alerter.types_codes.DefToMap;
import lots.jennifer_alerter.types_codes.TypeGroups;

import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.javaservice.jennifer.server.ErrorObject;

/**
 * 개발시 bottom-up 테스트용 앱.
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class TestApp implements Runnable {

	public static void main(String[] args) {
		new TestApp().run();
	}

	/*
	 * 실제 실행 내용.
	 * *******************************************************************
	 */

	@InjectLogger
	private Logger logger;

	@Inject
	private HelloMapper helloMapper;

	@Inject
	private SmsMapper smsMapper;

	@Inject
	private RecipientsMapper recipientsMapper;

	@Inject
	private ThundermailSender thundermailSender;

	@Inject
	private Provider<DbLoggingAlertActor> dbLoggingAlertActorProvider;

	@Override
	public void run() {
		Guicer.get().injectMembers(this);
		//
		// testOnePlusOne();
		// testSendSms();
		// testListAllRecipients();
		// testSendTextMail();

		// testTypeGroupsToMapByFieldNames();
		// testDefToMapToMapByFieldNames();

		// testDbLoggingActorInsert();

		testJmteExpanderWithErrorObject();

	}

	private void testJmteExpanderWithErrorObject() {
		ErrorObject error = new ErrorObject();
		error.group_type = 1;
		error.type = 2;
		error.time = 3;
		error.tx_uuid = 4;
		error.reqkey_hash = 5;
		error.skip_count = 6;
		error.agentname = "agentname-here";
		error.ipaddr = "ipaddr-this";
		error.msg = "msg-this";

		try {
			final String result = jmteExpander.expand(
					"/lots/jennifer_alerter/templates/test-utf8.jmte.txt",
					error, null);
			logger.debug(result);
		} catch (IOException e) {
			logger.error("JMTE EXPAND FAIL!", e);
		}
	}

	@Inject
	private JmteExpanderWithErrorObject jmteExpander;

	private void testDbLoggingActorInsert() {
		ErrorObject error = new ErrorObject();
		error.group_type = 1;
		error.type = 2;
		error.time = 3;
		error.tx_uuid = 4;
		error.reqkey_hash = 5;
		error.skip_count = 6;
		error.agentname = "agentname-here";
		error.ipaddr = "ipaddr-this";
		error.msg = "msg-this";
		dbLoggingAlertActorProvider.get().act(error, null);
	}

	private void testDefToMapToMapByFieldNames() {
		logger.debug(ObjectUtils.toString(DefToMap.toMapByFieldNames()));
	}

	private void testTypeGroupsToMapByFieldNames() {
		logger.debug(ObjectUtils.toString(TypeGroups.toMapByFieldNames()));
	}

	@Inject
	private PantechSms p;

	private void testSendTextMail() {
		try {
			thundermailSender.sendTextMail("테스트", "테스트입니다.", "나에게",
					"yun.jonghyouk@pantech.com");
		} catch (Exception e) {
			logger.error("SENDMAIL FAIL", e);
		}
	}

	private void testListAllRecipients() {
		logger.info(ObjectUtils.toString(recipientsMapper.listAll()));
	}

	private void testSendSms() {
		smsMapper.sendSms("0220300020", "01089217437", "TEST");
	}

	private void testOnePlusOne() {
		logger.info(String.format("1 + 1 = %s", helloMapper.onePlusOne()));
	}

}
