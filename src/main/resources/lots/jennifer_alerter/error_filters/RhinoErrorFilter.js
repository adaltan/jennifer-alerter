var agentname = error.agentname;
if(agentname){
	if(agentname.search(/^W|^S|^P/) >= 0){ 
		logger.debug("filter-ok -- agent matched " + agentname);
		true;
	}else{
		logger.debug("filter-fail -- agent not-matched " + agentname);
		false;
	}
}else{
	logger.debug("filter-ok -- no-agentname");
	true;
}