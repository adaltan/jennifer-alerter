package lots.jennifer_alerter.alert_actors;

import java.io.IOException;
import java.util.Map;

import lots.jennifer_alerter.logging.InjectLogger;
import lots.jennifer_alerter.templates.JmteExpanderWithErrorObject;
import lots.jennifer_alerter.thundermail.ThundermailSender;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.javaservice.jennifer.server.ErrorObject;

/**
 * 썬더메일 이메일 발송 경보 통지 행위자.
 * 
 * @author jhyun
 * @since 2012/11/28
 */
public class ThundermailAlertActor implements
		AlertActor<ThundermailAlertActor.ActContext> {

	public static class ActContext {
		private String toEmail;

		private String toName;

		public ActContext(String toEmail, String toName) {
			this.toEmail = toEmail;
			this.toName = toName;
		}

		public String getToEmail() {
			return toEmail;
		}

		public void setToEmail(String toEmail) {
			this.toEmail = toEmail;
		}

		public String getToName() {
			return toName;
		}

		public void setToName(String toName) {
			this.toName = toName;
		}
	}

	public static class ActContextMapper implements
			Function<Map, ActContext> {

		public ActContext apply(Map input) {
			if (input == null)
				return null;
			else {
				final String USER_NM_KEY = "USER_NM";
				final String toName = Strings.nullToEmpty((String) input
						.get(USER_NM_KEY));
				final String EMAIL_KEY = "EMAIL";
				final String toEmail = Strings.nullToEmpty((String) input
						.get(EMAIL_KEY));
				return new ActContext(toEmail, toName);
			}
		}
	}

	@InjectLogger
	private Logger logger;

	@Inject
	private ThundermailSender thundermailSender;

	@Inject
	private JmteExpanderWithErrorObject jmteExpander;

	private static final String TITLE_TEMPLATE_NAME = "/lots/jennifer_alerter/templates/thundermail-title.jmte.txt";

	private static final String CONTENT_TEMPLATE_NAME = "/lots/jennifer_alerter/templates/thundermail-content.jmte.txt";

	@Override
	public void act(ErrorObject errorObject,
			ThundermailAlertActor.ActContext actContext) {
		//
		String title = null;
		String content = null;
		try {
			title = jmteExpander.expand(TITLE_TEMPLATE_NAME, errorObject, null);
			content = jmteExpander.expand(CONTENT_TEMPLATE_NAME, errorObject,
					null);
		} catch (IOException e) {
			logger.error("JMTE EXPAND FAIL!", e);
		}
		// send
		try {
			thundermailSender.sendTextMail(title, content,
					actContext.getToName(), actContext.getToEmail());
		} catch (Exception exc) {
			logger.error("THUNDERMAIL SEND FAIL!", exc);
		}
	}

}
