package lots.jennifer_alerter.alert_actors;

import java.io.IOException;
import java.util.Map;

import lots.jennifer_alerter.config.AppConfig;
import lots.jennifer_alerter.logging.InjectLogger;
import lots.jennifer_alerter.sqlmaps.SmsMapper;
import lots.jennifer_alerter.templates.JmteExpanderWithErrorObject;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.mybatis.guice.transactional.Transactional;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.javaservice.jennifer.server.ErrorObject;

/**
 * SMS 전송 경보 알림 행위자.
 * 
 * @author jhyun
 * @since 2012/11/28
 */
public class SmsAlertActor implements AlertActor<SmsAlertActor.ActContext> {

	public static class ActContext {
		private String phoneNo;

		public ActContext(String phoneNo) {
			this.phoneNo = phoneNo;
		}

		public String getPhoneNo() {
			return phoneNo;
		}

		public void setPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
		}
	}

	public static class ActContextMapper implements
			Function<Map, ActContext> {
		public ActContext apply(Map input) {
			final String PPHONE_NO_KEY = "PPHONE_NO";
			if (input == null
					|| Strings.isNullOrEmpty((String) input.get(PPHONE_NO_KEY)))
				return null;
			else {
				final String phoneNo = ((String) input.get(PPHONE_NO_KEY))
						.replaceAll("\\-", "");
				return new ActContext(phoneNo);
			}
		}
	}

	@InjectLogger
	private Logger logger;

	@Inject
	private SmsMapper smsMapper;

	@Inject
	private JmteExpanderWithErrorObject jmteExpander;

	private static final String TEMPLATE_NAME = "/lots/jennifer_alerter/templates/sms.jmte.txt";

	@Transactional
	@Override
	public void act(ErrorObject errorObject, SmsAlertActor.ActContext actContext) {
		Configuration config = AppConfig.load();
		final String senderPhoneNo = config
				.getString("app.sms.sender-phone-no");
		//
		String message = null;
		try {
			message = jmteExpander.expand(TEMPLATE_NAME, errorObject, null);
		} catch (IOException e) {
			logger.error("JMTE EXPAND FAIL!", e);
		}
		//
		smsMapper.sendSms(senderPhoneNo, actContext.getPhoneNo(), message);
	}
}
