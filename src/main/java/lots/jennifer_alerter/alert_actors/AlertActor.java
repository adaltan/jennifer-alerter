package lots.jennifer_alerter.alert_actors;

import com.javaservice.jennifer.server.ErrorObject;

public interface AlertActor<T> {

	public void act(ErrorObject errorObject, T actContext);

}
