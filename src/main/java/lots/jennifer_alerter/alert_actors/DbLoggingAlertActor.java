package lots.jennifer_alerter.alert_actors;

import java.io.IOException;

import lots.jennifer_alerter.dates.SimpleDateFormatter;
import lots.jennifer_alerter.logging.InjectLogger;
import lots.jennifer_alerter.sqlmaps.DbLoggingMapper;
import lots.jennifer_alerter.templates.JmteExpanderWithErrorObject;
import lots.jennifer_alerter.types_codes.JenniferAlertTypesCodes;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.mybatis.guice.transactional.Transactional;

import com.google.inject.Inject;
import com.javaservice.jennifer.server.ErrorObject;

/**
 * DB에 경보 로깅 수행자.
 * 
 * @author jhyun
 * @since 2012/11/28
 */
public class DbLoggingAlertActor implements AlertActor<Object> {

	@InjectLogger
	private Logger logger;

	@Inject
	private DbLoggingMapper dbLoggingMapper;

	@Inject
	private JmteExpanderWithErrorObject jmteExpander;

	private static final String TEMPLATE_NAME = "/lots/jennifer_alerter/templates/db-logging-msg.jmte.txt";

	@Inject
	private JenniferAlertTypesCodes alertTypesCodes;

	@Inject
	private SimpleDateFormatter dateFmt;

	@Transactional
	@Override
	public void act(ErrorObject error, Object actContext) {
		String msg = error.msg;
		try {
			msg = jmteExpander.expand(TEMPLATE_NAME, error, null);
		} catch (IOException e) {
			logger.error("JMTE-EXPAND FAIL!", e);
		}
		final String typeGroupStr = alertTypesCodes.typeGroupsByValues
				.get(error.group_type);
		final String typeStr = alertTypesCodes.typesByValues.get(error.type);
		//
		dbLoggingMapper.insert(ObjectUtils.toString(typeGroupStr),
				ObjectUtils.toString(typeStr),
				dateFmt.epochDateFormat(error.time),
				ObjectUtils.toString(error.tx_uuid),
				ObjectUtils.toString(error.reqkey_hash),
				ObjectUtils.toString(error.skip_count),
				ObjectUtils.toString(error.agentname),
				ObjectUtils.toString(error.ipaddr), msg);
	}

}
