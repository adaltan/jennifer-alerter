package lots.jennifer_alerter.alert_actors;

import java.util.concurrent.ExecutorService;

import lots.jennifer_alerter.logging.InjectLogger;

import org.slf4j.Logger;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.javaservice.jennifer.server.ErrorObject;

/**
 * AlertActor을 후면에서 처리하도록 감싸는 행위자.
 * 
 * @author jhyun
 * @since 2012/11/28
 * 
 * @param <C>
 *            행위자에 넘길 문맥 객체 타입.
 * @param <A>
 *            행위자의 타입.
 */
public class BackgroundAlertActor<C, A extends AlertActor<C>> implements
		AlertActor<C> {

	@Inject
	private A actor;

	@InjectLogger
	private Logger logger;

	@Inject
	@Named("BackgroundAlertActorExecutorService")
	private ExecutorService execSvc;

	@Override
	public void act(final ErrorObject errorObject, final C actContext) {
		logger.debug(String.format("act with error=%s actContext=%s",
				errorObject, actContext));
		Runnable r = new Runnable() {
			@Override
			public void run() {
				actor.act(errorObject, actContext);
			}
		};
		execSvc.execute(r);
	}

	@Override
	public String toString() {
		return "BackgroundAlertActor [actor=" + actor + "]";
	}

}
