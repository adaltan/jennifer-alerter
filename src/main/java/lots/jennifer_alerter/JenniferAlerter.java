package lots.jennifer_alerter;

import java.util.List;
import java.util.Map;

import lots.jennifer_alerter.alert_actors.BackgroundAlertActor;
import lots.jennifer_alerter.alert_actors.DbLoggingAlertActor;
import lots.jennifer_alerter.alert_actors.SmsAlertActor;
import lots.jennifer_alerter.alert_actors.ThundermailAlertActor;
import lots.jennifer_alerter.config.AppConfig;
import lots.jennifer_alerter.error_filters.RhinoErrorFilter;
import lots.jennifer_alerter.functionals.IdentityFunction;
import lots.jennifer_alerter.logging.InjectLogger;
import lots.jennifer_alerter.sqlmaps.RecipientsMapper;
import lots.jennifer_alerter.types_codes.JenniferAlertTypesCodes;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.javaservice.jennifer.server.ErrorObject;

@Singleton
public class JenniferAlerter {

	@InjectLogger
	private Logger logger;

	@Inject
	private JenniferAlertTypesCodes typesCodes;

	@Inject
	private RecipientsMapper recipientsMapper;

	@Inject
	private AlertMatcher alertMatcher;

	@Inject
	private BackgroundAlertActor<SmsAlertActor.ActContext, SmsAlertActor> smsAlertActor;

	@Inject
	private SmsAlertActor.ActContextMapper smsAlertActorMapper;

	@Inject
	private BackgroundAlertActor<ThundermailAlertActor.ActContext, ThundermailAlertActor> thundermailAlertActor;

	@Inject
	private ThundermailAlertActor.ActContextMapper thundermailAlertActorMapper;

	@Inject
	private BackgroundAlertActor<Object, DbLoggingAlertActor> dbLoggingAlertActor;

	@Inject
	private IdentityFunction<Map, Object> identityMapper;

	@Inject
	private RhinoErrorFilter rhinoErrorFilter;

	private static Configuration config = null;

	private static List<Map> rcpts = null;

	public void initOnce() {
		if (config == null) {
			config = AppConfig.load();
		}
		if (rcpts == null) {
			rcpts = recipientsMapper.listAll();
			logger.debug(String.format("recipients = %s", rcpts));
		}
	}

	public void process(final ErrorObject error) {
		logger.debug(String.format("LOTS-ALERTER SENDSMS -- %s", error));
		//
		initOnce();
		//
		if (rhinoErrorFilter.filter(error)) {
			// group_type, type에 맞는 처리 방식들 찾아내기?
			final String groupTypeStr = typesCodes.typeGroupsByValues
					.get(error.group_type);
			final String typeStr = typesCodes.typesByValues.get(error.type);
			logger.debug(String.format("%s -- group[%s / %s] type[%s / %s]",
					error, error.group_type, groupTypeStr, error.type, typeStr));
			// sms, logging, email에 따라서 actor을 만들고 이들을 같은 레벨에서 적용이 가능하도록.
			// 타입그룹에 따라서.
			alertMatcher.matchAndApply(config, "app.type-groups.sms",
					error.group_type, typesCodes.typeGroupsByValues, error,
					rcpts, smsAlertActor, smsAlertActorMapper);
			alertMatcher.matchAndApply(config, "app.type-groups.email",
					error.group_type, typesCodes.typeGroupsByValues, error,
					rcpts, thundermailAlertActor, thundermailAlertActorMapper);
			alertMatcher.matchAndApply(config, "app.type-groups.logging",
					error.group_type, typesCodes.typeGroupsByValues, error,
					rcpts, dbLoggingAlertActor, identityMapper);
			// 타입에 따라서?
			alertMatcher.matchAndApply(config, "app.types.sms", error.type,
					typesCodes.typesByValues, error, rcpts, smsAlertActor,
					smsAlertActorMapper);
			alertMatcher.matchAndApply(config, "app.types.email", error.type,
					typesCodes.typesByValues, error, rcpts,
					thundermailAlertActor, thundermailAlertActorMapper);
			alertMatcher.matchAndApply(config, "app.types.logging", error.type,
					typesCodes.typesByValues, error, rcpts,
					dbLoggingAlertActor, identityMapper);
		}
	}

}
