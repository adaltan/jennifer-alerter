package lots.jennifer_alerter.templates;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lots.jennifer_alerter.dates.SimpleDateFormatter;
import lots.jennifer_alerter.logging.InjectLogger;
import lots.jennifer_alerter.types_codes.JenniferAlertTypesCodes;

import org.slf4j.Logger;

import com.google.inject.Inject;
import com.javaservice.jennifer.server.ErrorObject;

/**
 * 제니퍼 ErrorObject을 자동으로 바인딩해서 템플릿 확장.
 * 
 * groupTypeStr, typeStr.
 * 
 * @author jhyun
 * @since 2012/11/28
 */
public class JmteExpanderWithErrorObject {
	@Inject
	private JmteExpander expander;

	@Inject
	private JenniferAlertTypesCodes typesCodes;

	@InjectLogger
	private Logger logger;

	@Inject
	private SimpleDateFormatter dateFmt;

	public String expand(final String resourceName, ErrorObject error,
			final Map<String, Object> optionalBindings) throws IOException {
		//
		Map<String, Object> binding = new HashMap<String, Object>();
		if (optionalBindings != null) {
			binding.putAll(optionalBindings);
		}
		//
		final String groupTypeStr = typesCodes.typeGroupsByValues
				.get(error.group_type);
		binding.put("groupTypeStr", groupTypeStr);
		final String typeStr = typesCodes.typesByValues.get(error.type);
		binding.put("typeStr", typeStr);
		binding.put("timeStr", dateFmt.epochDateFormat(error.time));
		binding.put("error", error);
		//
		return expander.expand(resourceName, binding);
	}

}
