package lots.jennifer_alerter.templates;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;

import lots.jennifer_alerter.logging.InjectLogger;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.floreysoft.jmte.Engine;

/**
 * JMTE 템플릿 확장하기.
 * 
 * @author jhyun
 * @since 2012/11/28
 */
public class JmteExpander {

	@InjectLogger
	private Logger logger;

	public String expand(final String resourceName,
			final Map<String, Object> bindings) throws IOException {
		StringWriter stringWriter = new StringWriter();
		InputStream fileIn = this.getClass().getResourceAsStream(resourceName);
		IOUtils.copy(fileIn, stringWriter, Charset.forName("UTF-8"));
		fileIn.close();
		//
		Engine jmteEngine = new Engine();
		return jmteEngine.transform(stringWriter.toString(), bindings);
	}
}
