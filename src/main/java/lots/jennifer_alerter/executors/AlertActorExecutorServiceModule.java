package lots.jennifer_alerter.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.name.Names;

public class AlertActorExecutorServiceModule implements Module {

	private static final int nThreads = 5;

	@Override
	public void configure(Binder binder) {
		ExecutorService execSvc = Executors.newFixedThreadPool(nThreads);
		binder.bind(ExecutorService.class)
				.annotatedWith(
						Names.named("BackgroundAlertActorExecutorService"))
				.toInstance(execSvc);
	}
}
