package lots.jennifer_alerter.types_codes;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import lots.jennifer_alerter.functionals.MapUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 제니퍼 경보 타입의 그룹 코드 종류.
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class TypeGroups {
	private static Logger logger = LoggerFactory.getLogger(TypeGroups.class);

	public static final int CRITICAL = 1;
	public static final int ERROR = 2;
	public static final int WARNING = 3;
	public static final int MESSAGE = 4;

	public static Map<String, Integer> toMapByFieldNames() {
		Map<String, Integer> result = new HashMap<String, Integer>();
		Class<TypeGroups> klass = TypeGroups.class;
		for (Field field : klass.getFields()) {
			final String name = field.getName();
			int val = 0;
			try {
				val = field.getInt(klass);
			} catch (Exception e) {
				e.printStackTrace();
			}
			result.put(name, val);
		}
		return result;
	}

	public static Map<Integer, String> toMapByValues() {
		return MapUtils.swapMapKeyValue(toMapByFieldNames());
	}

}
