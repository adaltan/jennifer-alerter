package lots.jennifer_alerter.types_codes;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import lots.jennifer_alerter.functionals.MapUtils;
import lots.jennifer_alerter.functionals.StartsWithPredicate;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.javaservice.jennifer.util.Def;

/**
 * Def 클래스의 주요 타입 코드들을 Map으로 변환.
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class DefToMap {

	public static Map<String, Integer> toMapByFieldNames() {
		//
		final Predicate<String> ALLOWED_PREFIXES_PREDICATES = Predicates.or(
				new StartsWithPredicate("MESSAGE_"), new StartsWithPredicate(
						"WARNING_"), new StartsWithPredicate("ERROR_"),
				new StartsWithPredicate("CRITICAL_"));
		//
		Map<String, Integer> result = new HashMap<String, Integer>();
		Class<Def> klass = Def.class;
		for (Field field : klass.getFields()) {
			final String name = field.getName();
			if (ALLOWED_PREFIXES_PREDICATES.apply(name)) {
				try {
					final int val = field.getInt(klass);
					result.put(name, val);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	public static Map<Integer, String> toMapByValues() {
		return MapUtils.swapMapKeyValue(toMapByFieldNames());
	}

}
