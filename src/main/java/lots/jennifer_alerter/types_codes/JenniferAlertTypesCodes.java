package lots.jennifer_alerter.types_codes;

import java.util.Map;

import com.google.inject.Singleton;

/**
 * 제니퍼 경보 코드 모음.
 * 
 * @author jhyun
 * @since 2012/11/29
 */
@Singleton
public class JenniferAlertTypesCodes {

	/** 타입코드값(정수)로 타입코드 이름 문자열 찾기용 맵. */
	public final Map<Integer, String> typeGroupsByValues = TypeGroups
			.toMapByValues();

	/** 코드값(정수)로 코드 이름 문자열 찾기용 맵. */
	public final Map<Integer, String> typesByValues = DefToMap.toMapByValues();
}
