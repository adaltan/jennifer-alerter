package lots.jennifer_alerter.functionals;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;

/**
 * 문자열이 특정한 문자열로 시작하는가?
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class StartsWithPredicate implements Predicate<String> {

	private String prefix;

	public StartsWithPredicate(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public boolean apply(String input) {
		return Strings.nullToEmpty(input).startsWith(this.prefix);
	}

}
