package lots.jennifer_alerter.functionals;

import java.util.List;

import com.google.common.collect.Lists;

public class ListUtils {

	public static List<String> toStringsList(final List<? extends Object> l) {
		return Lists.transform(l, new ToStringFunction());
	}
}
