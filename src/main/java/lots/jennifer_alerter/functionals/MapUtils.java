package lots.jennifer_alerter.functionals;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jhyun
 * @since 2012/11/28
 */
public class MapUtils {

	/**
	 * Map의 키와 값을 서로 뒤집은 맵을 만들어줌.
	 * 
	 * @param m
	 * @return
	 */
	public static <K, V> Map<V, K> swapMapKeyValue(final Map<K, V> m) {
		Map<V, K> result = new HashMap<V, K>();
		for (K k : m.keySet()) {
			final V v = m.get(k);
			result.put(v, k);
		}
		return result;
	}
}
