package lots.jennifer_alerter.functionals;

import com.google.common.base.Function;

public class IdentityFunction<F, T> implements Function<F, T> {

	public T apply(F in) {
		return (T) in;
	}

}
