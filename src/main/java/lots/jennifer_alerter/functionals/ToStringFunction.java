package lots.jennifer_alerter.functionals;

import org.apache.commons.lang.ObjectUtils;

import com.google.common.base.Function;

public class ToStringFunction implements Function<Object, String> {

	public String apply(final Object o) {
		return ObjectUtils.toString(o);
	}
}
