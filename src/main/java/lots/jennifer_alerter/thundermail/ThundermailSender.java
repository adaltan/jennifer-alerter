package lots.jennifer_alerter.thundermail;

import lots.jennifer_alerter.config.AppConfig;
import lots.jennifer_alerter.logging.InjectLogger;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;

import tm.automail.sendapi.ThunderAutoMail;
import tm.automail.sendapi.ThunderAutoMailSender;

/**
 * Thundermail 메일 전송.
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class ThundermailSender {

	@InjectLogger
	private Logger logger;

	private Configuration config = AppConfig.load();

	/**
	 * 설정에서 메일서버, 보내는 사람, 보내는 이메일 정보를 얻어서 텍스트 메일 보내기.
	 * 
	 * @param title
	 * @param content
	 * @param toName
	 * @param toEmail
	 * @return
	 * @throws Exception
	 */
	public boolean sendTextMail(final String title, final String content,
			final String toName, final String toEmail) throws Exception {
		//
		final String MAIL_URL_KEY = "app.thundermail.server-url";
		final String mailUrl = config.getString(MAIL_URL_KEY);
		logger.debug(String.format("%s=[%s]", MAIL_URL_KEY, mailUrl));
		//
		final String FROM_NAME_KEY = "app.thundermail.from-name";
		final String fromName = config.getString(FROM_NAME_KEY);
		//
		final String FROM_EMAIL_KEY = "app.thundermail.from-email";
		final String fromEmail = config.getString(FROM_EMAIL_KEY);
		//
		final String TEMPLATE_ID_KEY = "app.thundermail.template-id";
		final String templateId = config.getString(TEMPLATE_ID_KEY);
		//
		return sendTextMail(mailUrl, templateId, title, content, fromName,
				fromEmail, toName, toEmail);
	}

	public boolean sendTextMail(final String mailUrl, final String templateId,
			final String title, final String content, final String fromName,
			final String fromEmail, final String toName, final String toEmail)
			throws Exception {
		//
		ThunderAutoMailSender tms = new ThunderAutoMailSender();
		ThunderAutoMail tm = new ThunderAutoMail();
		//
		tm.setThunderMailURL(mailUrl);
		tm.setAutomailID(templateId); // 템플릿ID, 1==기본 텍스트 메일.
		tm.setMailTitle(title); // 메일 제목
		tm.setMailContent(content); // 메일 내용
		tm.setSenderMail(fromEmail); // 보내는 사람 이메일
		tm.setSenderName(fromName); // 보내는 사람 이름
		tm.setReceiverName(toName); // 받는 사람 이름
		tm.setEmail(toEmail); // 받는 사람 이메일
		//
		return tms.send(tm); // 메일 발송(true / false)
	}

}
