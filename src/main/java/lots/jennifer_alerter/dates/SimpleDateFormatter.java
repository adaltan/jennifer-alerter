package lots.jennifer_alerter.dates;

import java.text.SimpleDateFormat;
import java.util.Date;

import lots.jennifer_alerter.logging.InjectLogger;

import org.slf4j.Logger;

public class SimpleDateFormatter {

	@InjectLogger
	private Logger logger;

	public String epochDateFormat(final long epoch) {
		try {
			final Date dt = new Date(epoch);
			final SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy/MM/dd HH:mm:ss");
			return sdf.format(dt);
		} catch (Exception exc) {
			logger.warn("DATE-FORMAT FAIL!", exc);
			return String.valueOf(epoch);
		}
	}

	public String epochDateFormat(final String epoch) {
		try {
			return this.epochDateFormat(Long.valueOf(epoch));
		} catch (Exception exc) {
			logger.warn("DATE-FORMAT FAIL!", exc);
			return epoch;
		}
	}
}
