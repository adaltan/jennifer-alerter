package lots.jennifer_alerter;

import java.util.List;
import java.util.Map;

import lots.jennifer_alerter.alert_actors.AlertActor;
import lots.jennifer_alerter.functionals.ListUtils;
import lots.jennifer_alerter.logging.InjectLogger;

import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.inject.Singleton;
import com.javaservice.jennifer.server.ErrorObject;

@Singleton
public class AlertMatcher {

	@InjectLogger
	public Logger logger;

	public <T> boolean match(final int code,
			final Map<Integer, String> codeMap, final List<String> patterns) {
		//
		final String codeStr = codeMap.get(code);
		logger.debug(String.format("code=[%s] codeStr=[%s] patterns=[%s]",
				code, codeStr, patterns));
		return patterns.contains(codeStr);
	}

	public <T> void applyAct(final ErrorObject error, List<Map> recipients,
			AlertActor<T> actor, Function<Map, ? extends Object> mapper) {
		//
		List<? extends Object> actContexts = Lists
				.transform(recipients, mapper);
		for (Object ac : actContexts) {
			actor.act(error, (T) ac);
		}
	}

	public <T> boolean matchAndApply(final Configuration config,
			final String configKey, final int code,
			final Map<Integer, String> codeMap, final ErrorObject error,
			final List<Map> recipients, AlertActor<T> actor,
			Function<Map, ? extends Object> mapper) {
		logger.debug(String.format("ENTER -- configKey=%s", configKey));
		final List<String> patterns = ListUtils.toStringsList(config
				.getList(configKey));
		if (match(code, codeMap, patterns)) {
			logger.debug(String.format(
					"MATCHED act %s -- code=[%s] patterns=%s", actor, code,
					patterns));
			applyAct(error, recipients, actor, mapper);
			return true;
		} else {
			logger.debug(String.format("NO-MATCHED -- code=[%s] patterns=%s",
					actor, code, patterns));
			return false;
		}
	}
}
