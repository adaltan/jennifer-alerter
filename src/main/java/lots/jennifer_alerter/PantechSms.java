package lots.jennifer_alerter;

import lots.jennifer_alerter.error_filters.RhinoErrorFilter;
import lots.jennifer_alerter.injections.Guicer;
import lots.jennifer_alerter.logging.InjectLogger;

import org.slf4j.Logger;

import com.google.inject.Inject;
import com.javaservice.jennifer.server.ErrorObject;
import com.javaservice.jennifer.server.Sendsms;
import com.javaservice.jennifer.util.Def;

/**
 * 실제 제니퍼 경보가 발생할때 호출되는 엔트리 클래스.
 * 
 * 구조에 대한 규격은 제니퍼 관련 레퍼런스 문서를 참고.
 * 
 * @author jhyun
 * @since 2012/11/28
 */
public class PantechSms implements Sendsms {
	@InjectLogger
	private Logger logger;

	@Inject
	private JenniferAlerter alerter;

	public void sendsms(ErrorObject errors[]) {
		for (ErrorObject error : errors) {
			alerter.process(error);
		}
	}

	public PantechSms() {
		Guicer.get().injectMembers(this);
		//
		logger.trace(String.format("LOTS-ALERTER CTOR -- %s", this));
	}
	
	@Inject
	public RhinoErrorFilter rhinoErrorFilter;
	

	public static void main(String[] args) {
		PantechSms p = new PantechSms();
		ErrorObject e1 = new ErrorObject();
		e1.group_type = 3; // WARNING
		e1.type = Def.WARNING_APP_BAD_RESPONSE;
		//p.sendsms(new ErrorObject[] { e1 });
		p.rhinoErrorFilter.filter(e1);
	}

}