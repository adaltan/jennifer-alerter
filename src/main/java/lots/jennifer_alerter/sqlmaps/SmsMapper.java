package lots.jennifer_alerter.sqlmaps;

import org.apache.ibatis.annotations.Param;

/**
 * SMS 발송.
 * 
 * @author jhyun
 * @since 2012-11-27
 */
public interface SmsMapper {

	void sendSms(@Param("senderPhoneNo") final String senderPhoneNo,
			@Param("destPhoneNo") final String destPhoneNo,
			@Param("message") final String message);

}
