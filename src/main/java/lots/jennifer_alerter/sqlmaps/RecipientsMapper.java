package lots.jennifer_alerter.sqlmaps;

import java.util.List;
import java.util.Map;

/**
 * 수신 대상자.
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public interface RecipientsMapper {

	/** 수신 대상자 목록. */
	List<Map> listAll();

}
