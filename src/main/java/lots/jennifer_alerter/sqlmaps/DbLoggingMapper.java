package lots.jennifer_alerter.sqlmaps;

import org.apache.ibatis.annotations.Param;

/**
 * DB 로깅.
 * 
 * @author jhyun
 * @since 2012-11-28
 */
public interface DbLoggingMapper {

	void insert(@Param("groupType") final String groupType,
			@Param("type") final String type, @Param("time") final String time,
			@Param("txUuid") final String txUuid,
			@Param("reqkeyHash") final String reqkeyHash,
			@Param("skipCount") final String skipCount,
			@Param("agentname") final String agentname,
			@Param("ipaddr") final String ipaddr, @Param("msg") final String msg);

}
