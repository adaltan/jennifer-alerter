package lots.jennifer_alerter.error_filters;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import lots.jennifer_alerter.logging.InjectLogger;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.perf4j.StopWatch;
import org.perf4j.slf4j.Slf4JStopWatch;
import org.slf4j.Logger;

import com.google.inject.Singleton;
import com.javaservice.jennifer.server.ErrorObject;

@Singleton
public class RhinoErrorFilter implements ErrorFilter {

	@InjectLogger
	private Logger logger;

	private static final String SCRIPT_PATH = "/lots/jennifer_alerter/error_filters/RhinoErrorFilter.js";

	@Override
	public boolean filter(ErrorObject error) {
		//
		try {
			initOnce();
		} catch (Exception exc) {
			logger.error("INIT FAIL!!!", exc);
		}
		//
		StopWatch sw = new Slf4JStopWatch(logger);
		sw.start();
		Context cx = Context.enter();
		sw.lap("rhino-js-context-entered");
		boolean result = false;
		try {
			Scriptable scope = cx.initStandardObjects();
			sw.lap("rhino-js-scope-inited");
			// binding: logger
			Object wrappedLogger = Context.javaToJS(logger, scope);
			ScriptableObject.putProperty(scope, "logger", wrappedLogger);
			// binding: error
			Object wrappedError = Context.javaToJS(error, scope);
			ScriptableObject.putProperty(scope, "error", wrappedError);
			//
			sw.lap("rhino-js-scope-puts-done");
			//
			Object evalResult = cx.evaluateReader(scope, scriptReader,
					SCRIPT_PATH, 1, null);
			sw.lap("rhino-js-eval-reader-done");
			logger.debug(String.format("eval done -- %s",
					Context.toString(evalResult)));
			result = Context.toBoolean(evalResult);
			sw.lap("rhino-js-get-result-done");
		} catch (Exception exc) {
			logger.error(
					String.format("FAIL EVALUATE SCRIPT -- %s", SCRIPT_PATH),
					exc);
		} finally {
			Context.exit();
			sw.lap("rhino-js-context-exited");
		}
		//
		return result;
	}

	private Reader scriptReader = null;

	private synchronized void initOnce() throws Exception {
		if (scriptReader == null) {
			StringWriter stringWriter = new StringWriter();
			InputStream in = this.getClass().getResourceAsStream(SCRIPT_PATH);
			IOUtils.copy(in, stringWriter);
			in.close();
			final String s = stringWriter.toString();
			scriptReader = new StringReader(s);
		} else {
			scriptReader.reset();
		}
	}
}
