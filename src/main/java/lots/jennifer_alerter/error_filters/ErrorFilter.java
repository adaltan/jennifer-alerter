package lots.jennifer_alerter.error_filters;

import com.javaservice.jennifer.server.ErrorObject;

public interface ErrorFilter {

	public boolean filter(final ErrorObject error);

}
