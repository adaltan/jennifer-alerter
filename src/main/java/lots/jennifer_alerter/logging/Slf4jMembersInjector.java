package lots.jennifer_alerter.logging;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.MembersInjector;

/**
 * SLF4J members-injector
 * 
 * 실제로 판명된 멤버 필드에 대해서 주입을 수행.
 * 
 * @author jhyun
 * @since 2012/11/27
 * 
 * @param <T>
 */
public class Slf4jMembersInjector<T> implements MembersInjector<T> {
	private final Logger logger;

	private final Field field;

	Slf4jMembersInjector(Field aField) {
		this.field = aField;
		this.logger = LoggerFactory.getLogger(field.getDeclaringClass());
		field.setAccessible(true);
	}

	public void injectMembers(T anArg0) {
		try {
			field.set(anArg0, logger);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}
