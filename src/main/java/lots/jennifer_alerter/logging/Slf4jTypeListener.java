package lots.jennifer_alerter.logging;

import java.lang.reflect.Field;

import org.slf4j.Logger;

import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

/**
 * SLF4J type-listener
 * 
 * 어떤 타입에 대해서 matching 여부를 판별. (어노테이션에 따라서 주입할지 안할지를 결정.)
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class Slf4jTypeListener implements TypeListener {

	public <I> void hear(TypeLiteral<I> aTypeLiteral,
			TypeEncounter<I> aTypeEncounter) {

		for (Field field : aTypeLiteral.getRawType().getDeclaredFields()) {
			if (field.getType() == Logger.class
					&& field.isAnnotationPresent(InjectLogger.class)) {
				aTypeEncounter.register(new Slf4jMembersInjector<I>(field));
			}
		}
	}

}
