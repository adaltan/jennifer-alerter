package lots.jennifer_alerter.logging;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.matcher.Matchers;

/**
 * SLF4J binding module
 * 
 * SLF4J 주입을 위한 바인딩 설정.
 * 
 * Injector을 만들때 포함할것.
 * 
 * @author jhyun
 * @since 2012/11/27
 */
public class Slf4jModule implements Module {

	@Override
	public void configure(Binder binder) {
		binder.bindListener(Matchers.any(), new Slf4jTypeListener());
	}

}
