

REM tm.automail / sendapi / 0.0.1-SNAPSHOT / thundermail_AutoAPI_UTF-8.jar
call mvn install:install-file -Dfile=artifacts\thundermail_AutoAPI_UTF-8.jar -DgroupId=tm.automail -DartifactId=sendapi -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar


REM com.javaservice.jennifer / jenniferserver / 0.0.1-SNAPSHOT / jenniferserver.jar
call mvn install:install-file -Dfile=artifacts\jenniferserver.jar -DgroupId=com.javaservice.jennifer -DartifactId=jenniferserver -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar


REM oracle / ojdbc14 / jdk14 / ojdbc-14.jar
call mvn install:install-file -Dfile=artifacts\ojdbc-14.jar -DgroupId=oracle -DartifactId=ojdbc14 -Dversion=jdk14 -Dpackaging=jar




REM DONE
pause